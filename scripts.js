var inputData = JSON.parse(JSON.stringify(finalJson, null, 2));
var ctx = document.getElementById("myChart");

// toSandDance(inputData);
toCSV(inputData);

var myChart = new Chart(ctx, {
    type: 'bar',
    data: toChartJs(inputData),
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});

function toChartJs(input) {
    var data = {
        labels: [],
        datasets: [
            {
                data: []
            }
        ],

    };

    var users = Object.keys(input.posts);
    var posts = [];
    users.forEach(function (user) {
        data.labels.push(user);
        posts = Object.keys(input.posts[user]);
        data.datasets[0].data.push(posts.length);
    });

    return data;
}

function toSandDance(input) {
    var users = Object.keys(input.posts);
    var output = [];
    var tempDate = undefined;
    users.forEach(function (userkey) {
        var posts = Object.keys(input.posts[userkey]);
        posts.forEach(function (post) {
            if ((input.posts[userkey][post]).ratings) {
                var ratings = Object.keys((input.posts[userkey][post]).ratings);
                ratings.forEach(function (rating) {
                    var record = {};

                    record.user = userkey;

                    record.text = input.posts[userkey][post].text;

                    if (input.posts[userkey][post].date) {
                        tempDate = new Date(input.posts[userkey][post].date);
                        tempDate.setHours(tempDate.getHours() + 2);
                        record.timestampPost = tempDate;
                    }
                    record.timestampRating = rating;
                    // console.log(record.timestampRating);

                    record.rating = input.posts[userkey][post].ratings[rating];
                    output.push(record);
                });
            } else {
                var record = {};

                record.user = userkey;

                record.text = input.posts[userkey][post].text;
                if (input.posts[userkey][post].date) {
                    var tempDate = new Date(input.posts[userkey][post].date);
                    tempDate.setHours(tempDate.getHours + 2);
                    record.timestampPost = tempDate;
                }

                output.push(record);
            }
        });
    });
    console.log(JSON.stringify(output, null, 2));
}

function toCSV(input) {
    var participants = {
        "1": {
            participant: "P1",
            gender: "Male",
            age: "?",
            socialStatus: "?"
        },
        "2": {
            participant: "P2",
            gender: "Female",
            age: "?",
            socialStatus: "?"
        },
        "3": {
            participant: "P3",
            gender: "Female",
            age: "?",
            socialStatus: "?"
        },
        "4": {
            participant: "P4",
            gender: "Female",
            age: "?",
            socialStatus: "?"
        },
        "5": {
            participant: "P5",
            gender: "Male",
            age: "?",
            socialStatus: "?"
        },
        "6": {
            participant: "P6",
            gender: "Male",
            age: "?",
            socialStatus: "?"
        }
    };

    function getPostTimestampValues(postTime) {
        var result = {};
        var tempDate = new Date(postTime);
        tempDate.setHours(tempDate.getHours() + 2);
        switch (tempDate.getDay()) {
            case 0:
                result.weekday = "Sun";
                break;
            case 1:
                result.weekday = "Mon";
                break;
            case 2:
                result.weekday = "Tue";
                break;
            case 3:
                result.weekday = "Wed";
                break;
            case 4:
                result.weekday = "Thu";
                break;
            case 5:
                result.weekday = "Fri";
                break;
            case 6:
                result.weekday = "Sat";
                break;
            default:
                result.weekday = "error";
        }
        var ISOString = tempDate.toISOString();
        ISOString = ISOString.substring(0, postTime.indexOf("."));
        ISOString = ISOString + "+02:00";
        result.time = ISOString.substring(11, 29);
        result.timeClock = ISOString.substring(11, 16);
        result.date = ISOString.substring(0, 4) + "/" + ISOString.substring(5, 7) + "/" + ISOString.substring(8, 10);
        result.timestamp = ISOString;
        return result;
    }

    function getRatingTimestampValues(ratingTime) {
        var result = {};
        var year = ratingTime.substring(ratingTime.length - 4, ratingTime.length);
        var month = "";
        result.weekday = ratingTime.substring(0, 3);
        var inputMonth = ratingTime.substring(4, 7);
        switch (inputMonth) {
            case "Apr":
                month = "04";
                break;
            case "May":
                month = "05";
                break;
            default:
                month = "error";
        }
        var day = ratingTime.substring(8, 10);
        var time = ratingTime.substring(11, 19);
        result.time = time;
        var timeClock = ratingTime.substring(11, 16);
        result.timeClock = timeClock;
        result.timestamp = year + "-" + month + "-" + day + "T" + time + "+02:00";
        var date = year + "/" + month + "/" + day;
        result.date = date;
        return result;

    }

    var output = "Participant;Gender;Age;Social Status;Post Text;Post Timestamp;Post time;Post date;Post Weekday;Rating;Rating Timestamp;Rating time;Rating date;Rating Weekday;\n";
    var users = Object.keys(input.posts);
    users.forEach(function (userkey) {
        var posts = Object.keys(input.posts[userkey]);
        posts.forEach(function (post) {
            if ((input.posts[userkey][post]).ratings) {
                var ratings = Object.keys((input.posts[userkey][post]).ratings);
                ratings.forEach(function (rating) {
                    var participant = participants[userkey].participant;
                    var gender = participants[userkey].gender;
                    var age = participants[userkey].age;
                    var socialStatus = participants[userkey].socialStatus;
                    var postText = input.posts[userkey][post].text;
                    var postTimestampValues = getPostTimestampValues(input.posts[userkey][post].date);
                    var postTimestamp = postTimestampValues.timestamp;
                    var postTime = postTimestampValues.timeClock;
                    var postDate = postTimestampValues.date;
                    var postWeekday = postTimestampValues.weekday;
                    var ratingValue = input.posts[userkey][post].ratings[rating];
                    var ratingTimestampValues = getRatingTimestampValues(rating);
                    var ratingTimestamp = ratingTimestampValues.timestamp;
                    var ratingTime = ratingTimestampValues.timeClock;
                    var ratingDate = ratingTimestampValues.date;
                    var ratingWeekday = ratingTimestampValues.weekday;
                    output = output + participant + ";" + gender + ";" + age + ";" + socialStatus + ";" + postText + ";" + postTimestamp +
                        ";" + postTime + ";" + postDate + ";" + postWeekday + ";" + ratingValue + ";" + ratingTimestamp + ";" + ratingTime + ";" + ratingDate + ";" + ratingWeekday + "\n";
                });
            } else {
                var participant = participants[userkey].participant;
                var gender = participants[userkey].gender;
                var age = participants[userkey].age;
                var socialStatus = participants[userkey].socialStatus;
                var postText = input.posts[userkey][post].text;
                var postTimestampValues = getPostTimestampValues(input.posts[userkey][post].date);
                var postTimestamp = postTimestampValues.timestamp;
                var postTime = postTimestampValues.timeClock;
                var postDate = postTimestampValues.date;
                var postWeekday = postTimestampValues.weekday;
                var ratingValue = "N/A";
                var ratingTimestamp = "N/A";
                var ratingTime = "N/A";
                var ratingDate = "N/A";
                var ratingWeekday = "N/A";
                output = output + participant + ";" + gender + ";" + age + ";" + socialStatus + ";" + postText + ";" + postTimestamp +
                    ";" + postTime + ";" + postDate + ";" + postWeekday + ";" + ratingValue + ";" + ratingTimestamp + ";" + ratingTime + ";" + ratingDate + ";" + ratingWeekday + "\n";

            }
        });
    });
    console.log(output);
}